I got tired of rewriting this function to retrieve secrets from our Secrets Manager, so here is a python script to make it so that nobody has to copy and paste the pre-generated aws code. 

The function takes two parameters, `secret_name` and `region_name`. 

Whatever you named the secret in Secrets Manager and whatever region the secret is in, replace those with `secret_name` and `region_name` respectively. 
